<%-- 
    Document   : home
    Created on : Apr 21, 2020, 11:25:32 AM
    Author     : urosv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
        <%@include file="template/header.jsp" %>
        <p></p>
        <%@include file="template/sidebar.jsp" %>
        
        <h1>Welcome to Home page !</h1>
        
        <%@include file="template/footer.jsp" %>
    </body>
</html>
