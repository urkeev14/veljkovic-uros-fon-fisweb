<%-- 
    Document   : sidebar
    Created on : Apr 22, 2020, 4:01:47 PM
    Author     : urosv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Hello ${sessionScope.loginUser.email} ! 
        <div>
            <a href="#">Logout</a>
        </div>
        <div>
            <a href="/uvfon/app/user/all_departments"> All departments</a>
        </div>
        <div>
            <a href="/uvfon/app/user/add_department"> Add department</a>
        </div>

    </body>
</html>
