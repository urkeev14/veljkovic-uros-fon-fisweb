<%-- 
    Document   : login
    Created on : Apr 21, 2020, 10:46:39 AM
    Author     : urosv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>

    <body>
        <h1>This is login page !</h1>
        ${message}
        <form action="/uvfon/app/login" method="post">
            Email:
            <div>
                <input type = "text" id="email" name = "email"/>
            </div>
            Password
            <div>
                <input type = "password" id="password" name = "password"/>
            </div>
            <p/>
            <input type = "submit" id = "Login" value = "Log in"/>
        </form>
    </body>

</html>
