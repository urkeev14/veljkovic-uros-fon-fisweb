/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.listener;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import veljkovic.uros.fon.fisweb.model.User;

/**
 *
 * @author urosv
 */
@WebListener
public class MyAppContextListener implements ServletContextListener {

    public MyAppContextListener() {
        System.out.println("======================================================");
        System.out.println("========= MyApplicationContextListener=================");
        System.out.println("=======================================================");
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("======================================================");
        System.out.println("=========        contextInitialized  =================");
        System.out.println("=======================================================");
        
        sce.getServletContext().setAttribute("users", createUsers());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    private List<User> createUsers() {
        return new ArrayList<User>() {
            {
                add(new User("pera", "peric", "pera@gmail.com", "pera"));
                add(new User("uros", "uros", "uros@gmail.com", "uros"));
                add(new User("asdf", "asdf", "asdf@gmail.com", "asdf"));
            }
        };
    }

}
