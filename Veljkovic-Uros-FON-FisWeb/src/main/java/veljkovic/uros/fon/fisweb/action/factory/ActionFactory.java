/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.action.factory;

import veljkovic.uros.fon.fisweb.action.AbstractAction;
import veljkovic.uros.fon.fisweb.action.impl.AddDepartmentAction;
import veljkovic.uros.fon.fisweb.action.impl.AllDepartmentsAction;
import veljkovic.uros.fon.fisweb.action.impl.LoginAction;
import veljkovic.uros.fon.fisweb.constants.ActionConstants;
import veljkovic.uros.fon.fisweb.constants.PageConstants;

/**
 *
 * @author urosv
 */
public class ActionFactory {

    public static AbstractAction createActionFactory(String actionName) {
        AbstractAction action = null;

        switch (actionName) {
            case ActionConstants.URL_LOGIN:
                action = new LoginAction();
                break;
            case ActionConstants.URL_ADD_DEPARTMENT:
                action = new AddDepartmentAction();
                break;
            case ActionConstants.URL_ALL_DEPARTMENTS:
                action = new AllDepartmentsAction();
                break;
            default:
                return null;
        }
        return action;
    }

}
