/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.constants;

/**
 *
 * @author urosv
 */
public interface ActionConstants {
    
    public static final String URL_LOGIN = "/login";
    public static final String URL_ALL_DEPARTMENTS = "/user/all_departments";
    public static final String URL_ADD_DEPARTMENT = "/user/add_department";
    
}
