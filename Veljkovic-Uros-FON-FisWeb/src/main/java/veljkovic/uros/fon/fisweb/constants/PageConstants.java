/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.constants;

/**
 *
 * @author urosv
 */
public interface PageConstants {

    public static final String VIEW_LOGIN = "VIEW_LOGIN";
    public static final String VIEW_HOME = "VIEW_HOME";
    public static final String VIEW_ADD_DEPARTMENT = "VIEW_ADD_DEPARTMENT";
    public static final String VIEW_ALL_DEPARTMENTS = "VIEW_ALL_DEPARTMENTS";
    
    public static final String VIEW_DEFAULT_ERROR = "VIEW_DEFAULT_ERROR";

    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_HOME = "/WEB-INF/pages/home.jsp";
    public static final String PAGE_ADD_DEPARTMENT = "/WEB-INF/pages/user/add_department.jsp";
    public static final String PAGE_ALL_DEPARTMENTS = "/WEB-INF/pages/user/all_departments.jsp";
    
    public static final String PAGE_DEFAULT_ERROR = "/WEB-INF/pages/error/error.jsp";
    
}
