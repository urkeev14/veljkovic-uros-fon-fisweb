/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.action.impl;

import com.sun.corba.se.spi.presentation.rmi.StubAdapter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import veljkovic.uros.fon.fisweb.action.AbstractAction;
import veljkovic.uros.fon.fisweb.constants.PageConstants;
import veljkovic.uros.fon.fisweb.model.User;

/**
 *
 * @author urosv
 */
public class LoginAction implements AbstractAction {

    public String execute(HttpServletRequest request) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        System.out.println("====================================================");
        System.out.println("====================LoginAction=====================");
        System.out.println(email + "\t" + password);
        System.out.println("====================================================");

        User user = findUser(request, email);
        if (user == null) {
            request.setAttribute("message", "User does not exist !");
            return PageConstants.VIEW_LOGIN;
        } else {
            request.getSession(true).setAttribute("loginUser", user);
            return PageConstants.VIEW_HOME;
        }
    }

    private User findUser(HttpServletRequest request, String email) {
        List<User> users = (List<User>) request.getServletContext().getAttribute("users");
        for (User user : users) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }
        return null;
    }

}
