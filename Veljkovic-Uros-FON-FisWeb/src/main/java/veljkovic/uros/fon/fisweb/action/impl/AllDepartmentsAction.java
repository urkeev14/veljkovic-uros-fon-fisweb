/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.action.impl;

import javax.servlet.http.HttpServletRequest;
import veljkovic.uros.fon.fisweb.action.AbstractAction;
import veljkovic.uros.fon.fisweb.constants.ActionConstants;
import veljkovic.uros.fon.fisweb.constants.PageConstants;

/**
 *
 * @author urosv
 */
public class AllDepartmentsAction implements AbstractAction{

    @Override
    public String execute(HttpServletRequest request) {
        return PageConstants.VIEW_ALL_DEPARTMENTS;
    }
    
}
