/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veljkovic.uros.fon.fisweb.controller;

import javax.servlet.http.HttpServletRequest;
import veljkovic.uros.fon.fisweb.action.AbstractAction;
import veljkovic.uros.fon.fisweb.action.factory.ActionFactory;
import veljkovic.uros.fon.fisweb.constants.PageConstants;

/**
 *
 * @author urosv
 */
public class ApplicationController {

    public String processRequest(String pathInfo, HttpServletRequest request) {
        String nextPage = PageConstants.VIEW_DEFAULT_ERROR;

        AbstractAction action = ActionFactory.createActionFactory(pathInfo);
        if (action != null) {
            nextPage = action.execute(request);
        }
        return nextPage;

    }

}
